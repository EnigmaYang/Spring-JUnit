package com.marlabs.test;

import javax.annotation.Resource;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.marlabs.business.Function1;

@RunWith(SpringJUnit4ClassRunner.class)

// default value is in same package and with name "ClassName-context.xml"
@ContextConfiguration(locations = "classpath:springTest-beans.xml")
public class TestDemo {

	@Resource
	Function1 function1;

	@Test
	public void addTest() {
		assertEquals(100, function1.add(10, 90));
	}

}
