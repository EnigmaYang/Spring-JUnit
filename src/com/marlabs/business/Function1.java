package com.marlabs.business;

import org.springframework.stereotype.Component;

@Component
public class Function1 {
	public int add(int a, int b) {
		return a + b;
	}

	public int min(int a, int b) {
		return a - b;
	}

	public int mut(int a, int b) {
		return a * b;
	}

	public int div(int a, int b) {
		return a / b;
	}
}
